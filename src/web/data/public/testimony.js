$(function() {
  	$.get( "viewtestimony.php", function( data ) {
	  	results = JSON.parse(data);
	  	var ol = "<ol class='carousel-indicators'>";
	  	var li = "<li data-target='#myCarousel' data-slide-to='0' class='active'></li>";
	  	var div = "<div class='carousel-inner'><div class='item carousel-item active'><div class='img-box'></div><p class='testimonial'>"+ results[0].comment + "</p><p class='overview'><b>" + results[0].name +"</b>, " + results[0].position+ "</b> @ "+ results[0].company +"</p></div>";

		for (var i = 1 ; i < results.length; i++) {
			
	  	 	li = li + "<li data-target='#myCarousel' data-slide-to='" + i + "'></li>";

			div= div + "<div class='item carousel-item '><div class='img-box'></div><p class='testimonial'>"+ results[i].comment + "</p><p class='overview'><b>" + results[i].name +"</b>, " + results[i].position+ "</b> @ "+ results[i].company +"</p></div>";
		}
		ol = ol + li + "</ol>";
		div = div + "</div>";
document.getElementById("myCarousel").innerHTML =  ol + div +   
      "<a class='left carousel-control' href='#myCarousel' data-slide='prev'><span class='glyphicon glyphicon-chevron-left'></span><span class='sr-only'>Previous</span></a><a class='right carousel-control' href='#myCarousel' data-slide='next'><span class='glyphicon glyphicon-chevron-right'></span><span class='sr-only'>Next</span> </a>";
  	});
});

$( "#testimonyform" ).submit(function( event ) {

	event.preventDefault();
	var $form = $(this),
		url = $form.attr( "action" ),
		values = $(this).serialize();

	var posting = $.post( url, 
			values
			
		);

	posting.done(function( data ) {
		$( "#testimonyform" ).trigger('reset');
		alert("Votre commentaire a été soumis")
	});

});